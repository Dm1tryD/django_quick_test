from django.views.generic import ListView
from django.db.models import *
from .models import Production, ProductionProduced


class ProductReports(ListView):

    model = Production
    template_name = 'myfirstapp/product_reports.html'
    context_object_name = 'reports'


class ProdProduced(ListView):

    model = ProductionProduced
    template_name = 'myfirstapp/product.html'

    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get_queryset(self):
        return ProductionProduced.objects.select_related('prod_report').prefetch_related('productionconsumed_set').\
            filter(prod_report_id=self.kwargs['report_id'])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        queryset = context['object_list']
        product = {}
        total_price = 0
        for produced in queryset:
            consumed_items = []
            summary_price = 0
            for consumed in produced.productionconsumed_set.all():
                matirial_name = consumed.consumed
                consumed_uom = consumed.consumed_uom
                quantity = consumed.quantity
                one_material_price = consumed.average_price * quantity
                summary_price += one_material_price
                consumed_items.append({'consumed': matirial_name, 'consumed_uom': consumed_uom, 'quantity': quantity,
                                       'price': one_material_price
                                       })
            total_price += summary_price
            product.update(
                {produced: {'produced': produced, 'consumed': consumed_items, 'summary_price': summary_price}}
            )
        products = {'product': product, 'total_price': total_price}
        context.update(products)
        return context
