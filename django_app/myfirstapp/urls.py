from django.urls import path
from .views import ProductReports, ProdProduced

urlpatterns = [
    path('prodreports/', ProductReports.as_view(), name='prodreports'),
    path('production/<int:report_id>/', ProdProduced.as_view(), name='production')
]